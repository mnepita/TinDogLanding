## Tin Dog

A fully responsive startup landing page for "Tin Dog", a tinder for dogs. 

**Technologies:**
- HTML
- CSS3
- Boostrap

[Visit Tin Dog](https://mnepita.github.io/TinDogLanding)

Project for the WebDeveloper Bootcamp from the [AppBrewery](https://www.appbrewery.co/) [Angela Yu](https://twitter.com/yu_angela).


[Martin Nepita | mnepita.me](https://mnepita.me)
